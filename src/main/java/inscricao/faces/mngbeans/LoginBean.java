/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.faces.mngbeans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import java.util.Date;
/**
 *
 * @author lucas
 */

@ManagedBean(name="loginBean", eager=true)
//@RequestScoped
@ApplicationScoped
public class LoginBean {
    
    private String usuario;
    private String senha;
    private Boolean admin;
    
    private List<ArrayList<String>> logins = new ArrayList<ArrayList<String>>();
    ArrayList<String> usuario_hora = new ArrayList<String>();
    
    Date date = new Date();
    DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
    
    public void setList(List<ArrayList<String>> logins)
    {
        this.logins = logins;
    }
    
    public List<ArrayList<String>> getLogins()
    {
        return logins;
    }
    
    public void addUsuario()
    {
        usuario_hora.add(usuario);
        usuario_hora.add(df.format(date));
        logins.add(usuario_hora);
    }
    
    public String getUsuario()
    {
        return usuario;
    }
    
    public void setUsuario(String usuario)
    {
        this.usuario = usuario;
    }
    
    public String getSenha()
    {
        return senha;
    }
    
    public void setSenha(String senha)
    {
        this.senha = senha;
    }
    
    public Boolean getAdmin()
    {
        return admin;
    }
    
    public void setAdmin(Boolean admin)
    {
        this.admin = admin;
    }
    
    public String login()
    {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        if(usuario.equals(senha))
        {
            addUsuario();
            
            if(admin)
            {
                return "admin";
            }
            else
                return "cadastro";
            
        }
        else
        {
            facesContext.addMessage("form", new FacesMessage("Acesso negado"));
            return null;
        }
      }
}